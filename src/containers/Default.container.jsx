import React from "react";

// libraries
import styled from 'styled-components';
import {
  motion,
  useTransform,
} from "framer-motion"

// design
import colors from '../design/colors';
import Mark from '../design/mark_main.png'

const DefaultContainer = ({y, primary, children}) => {
 
  const backgroundColor = useTransform(
    y,
    [-100, 0, 100],
    [colors.PINK, colors.PURPLE, colors.ORANGE]
  )

  return (
    <Container style={{ backgroundColor: backgroundColor }} id='default-container' primary={primary}>
      <BackgroundLogo src={Mark} alt="oomrazoom-logo-small" />
      {children}
    </Container>
  )
};

const Container = styled(motion.div)`
  z-index: 1;
  height: ${document.getElementById('root').offsetHeight + 'px'};
  width: ${document.getElementById('root').offsetWidth + 'px'};
  display: flex;
  flex-direction: column;
  justify-content: ${props => props.primary === 'contact' ? 'flex-start' : props.primary === 'portfolio' ? 'flex-end' : 'space-between'};
  align-items: center;
  overflow: hidden;
  border-radius: 4px;
`

const BackgroundLogo = styled.img`
  position: absolute;
  opacity: .08;
  height: 20rem;
  top: calc(50% - 10rem);
`;

export default DefaultContainer;
