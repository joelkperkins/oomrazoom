import React from 'react';
import { Drawer, Input } from 'antd';
import styled from 'styled-components';
import {FiAtSign} from 'react-icons/fi'
import {MdPerson} from 'react-icons/md'
import {IoIosSend} from 'react-icons/io'


const EmailDrawer = ({
  drawerOpen,
  onClose,
  name,
  setName,
  email,
  setEmail,
  message,
  setMessage,
  sendContact
}) => {
  const { TextArea } = Input;
  
  return (
      <Drawer
        title="Contact Oomrazoom!"
        placement="bottom"
        closable={true}
        onClose={onClose}
        visible={drawerOpen}
      >
        <Container>
          <Input placeholder="name" prefix={<MdPerson size='1rem' color='black' />} value={name} onChange={(e) => setName(e.target.value)} />
          <Input placeholder="email" prefix={<FiAtSign size='1rem' color='black' />} value={email} onChange={(e) => setEmail(e.target.value)} />
          <TextArea placeholder="Your message here..." allowClear value={message} onChange={(e) => setMessage(e.target.value)} />
          <IoIosSend size='3rem' color='black' onClick={() => sendContact()} />
        </Container>
      </Drawer>
  );
};

const Container = styled.div`
  height: 100%;
  width: 100%;

  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  align-items: center;
`

export default EmailDrawer;