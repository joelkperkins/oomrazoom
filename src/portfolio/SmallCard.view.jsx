import React, {useRef} from "react";
import styled from 'styled-components';
import colors from '../design/colors';
import {AiOutlineGitlab, AiOutlineGithub, AiOutlineLink} from 'react-icons/ai';

const SmallCardView = ({ logo, name, url, repo }) => {

  const windowRef = useRef(null);

  return (
    <SmallCard onClick={() => {
      windowRef.current = window.open(url, '_blank');
      windowRef.current.focus();
      windowRef.current = null;
    }}>
      <SmallLogo src={logo} alt='project logo' />
      <Row width='90%' justify='center'>
        <Row width='85%' justify='space-evenly'>
          <SmallTitle>{name}</SmallTitle>
          <AiOutlineLink
            size="2rem"
            color="black"
            onClick={() => {
              windowRef.current = window.open(url, '_blank');
              windowRef.current.focus();
              windowRef.current = null;
            }}
          />
          {repo.logo === 'gitlab' && 
            <AiOutlineGitlab
              size="2rem"
              color="black"
              onClick={() => {
                windowRef.current = window.open(repo, '_blank');
                windowRef.current.focus();
                windowRef.current = null;
              }}
            />
          }
          {repo.logo === 'github' && 
            <AiOutlineGithub
              size="2rem"
              color="black"
              onClick={() => {
                windowRef.current = window.open(repo, '_blank');
                windowRef.current.focus();
                windowRef.current = null;
              }}
            />
          }
        </Row>
      </Row>
    </SmallCard>
  )
  };

const SmallCard = styled.div`
  margin-bottom: .5rem;
  height: 5.5rem;
  width: 90%;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 4px;

  background-color: ${colors.YELLOW};
`

const Row = styled.div`
  width: ${props => props.width};
  display: flex;
  align-items: center;
  justify-content: ${props => props.justify};
  flex-wrap: wrap;
`

const SmallTitle = styled.div`
  font-family: 'Lilita One', sans-serif;
  font-size: 22px;
  min-width: 55%;
`

const SmallLogo = styled.img`
  padding-left: 1rem;
  width: 4rem;
`

export default SmallCardView;
