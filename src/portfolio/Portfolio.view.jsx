import React from "react";
import { useHistory } from "react-router-dom";
import styled from 'styled-components';
import { motion } from "framer-motion"
import colors from '../design/colors';
import {FaCodeBranch} from 'react-icons/fa';

// views
import SmallCardView from './SmallCard.view';

// images
import petpro from '../design/images/petpro.png'

const PortfolioView = ({ className, controls, primary }) => {

  const history = useHistory();

  const projects = [
    {
      name: 'Pet Pro Tele+',
      logo: petpro,
      repo: {
        url: 'https://gitlab.com/users/joelkperkins/contributed',
        logo: 'gitlab'
      },
      url: 'https://www.petproconnect.com/teleplus',
    },
    {
      name: 'SBC: Radio',
      logo: 'https://raw.githubusercontent.com/joelkperkins/Soft-Boys-Club-Radio/master/public/favicon-32x32.png',
      repo: {
        url:'https://github.com/joelkperkins/Soft-Boys-Club-Radio',
        logo: 'github'
      },
      url: 'https://www.softboys.club/',
    }
  ];

  const Projects = projects.map((project, index) => {
    return (
      <SmallCardView
        key={`project-${index}`}
        logo={project.logo}
        name={project.name}
        url={project.url}
        repo={project.repo}
      />
    )
  });

  if (primary !== 'portfolio') {
    return (
      <Container 
        id="portfolio"
        className={className}
        animate={controls}
        whileHover={{ scale: 1.1 }}
        initial={false}
        onClick={() => history.push('/portfolio')}
      >
        <ButtonContainer>
          <FaCodeBranch size="3rem" color={colors.WHITE} />
          <Title>Portfolio</Title>
        </ButtonContainer>
      </Container>
    )
  } else {
    return (
      <Container 
        id="portfolio"
        className={className}
        animate={controls}
        initial={false}
      >

        <ProjectContainer>
          {Projects}
        </ProjectContainer>

        <ButtonContainer>
          <FaCodeBranch size="3rem" color={colors.WHITE} />
          <Title>Portfolio</Title>
        </ButtonContainer>

      </Container>
    )
  }
};

// main button
const Container = styled(motion.div)`
  z-index: 3;
  width: 100%;
  height: 100%;
  
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  align-items: center;

  background: ${colors.ORANGE};
  border-radius: 4px;
  border: none;
`

const ButtonContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  margin-bottom: 1.5rem;
`

const ProjectContainer = styled.div`
  width: 100%;
  height: 60%;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
`

const Title = styled.div`
  font-family: 'Lilita One', sans-serif;
  font-size: 48px;
  color: ${colors.WHITE};

  &:hover {
    cursor: pointer;
  }
`

export default PortfolioView;
