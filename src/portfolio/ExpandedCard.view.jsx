import React, { useState, useEffect, useRef } from "react";
import styled from 'styled-components';
import colors from '../design/colors';
import {AiOutlineGitlab, AiOutlineLink, AiOutlineArrowLeft, AiOutlineArrowRight} from 'react-icons/ai';

const ExpandedCardView = ({ logo, name, url, repo, pictures, setExpanded, desc }) => {
  const [active, setActive] = useState(0);

  const intervalRef = useRef(null);

  useEffect(() => {
    intervalRef.current = setInterval(() => {
      setActive(active === pictures.length ? 0 : active + 1)
    }, 1500);

    return () => clearInterval(intervalRef.current);
  }, []);

  return (
    <ExpandedCard>
      <TitleArea onClick={() => setExpanded(false)}>
        <SmallLogo src={logo} alt='project logo' />
        <SmallTitle>{name}</SmallTitle>
        <AiOutlineLink
          size="2rem"
          color="black"
          onClick={() => {
            const win = window.open(url, '_blank');
            win.focus();
          }}
        />
        <AiOutlineGitlab
          size="2rem"
          color="black"
          onClick={() => {
            const win = window.open(repo, '_blank');
            win.focus();
          }}
        />
      </TitleArea>

      <Carousel>
        <div onClick={() => {
          setActive(active === 0 ? pictures.length : active - 1)
          clearInterval(intervalRef.current);
        }}>
          <AiOutlineArrowLeft size='2rem' color={colors.PURPLE} />
        </div>
        <div style={{ padding: '0 .25rem'}}>
          {active === 0 && <Image src={pictures[active]} alt='app-demo' />}
          {active === 1 && <Image src={pictures[active]} alt='app-demo' />}
          {active === 2 && <Image src={pictures[active]} alt='app-demo' />}
          {active === pictures.length &&
            <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'space-evenly', alignItems: 'flex-start'}} onClick>
              {desc.map((item, index) => {
                return (
                  <TextRender key={'item' + index}>{item}</TextRender>
                )
              })}
            </div>
          }
        </div>
        <div onClick={() => {
          setActive(active === pictures.length ? 0 : active + 1);
          clearInterval(intervalRef.current);
        }}>
          <AiOutlineArrowRight size='2rem' color={colors.PURPLE} />
        </div>
      </Carousel>

    </ExpandedCard>
  )
  };

const ExpandedCard = styled.div`
  height: 20rem;
  width: 90%;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  border-radius: 4px;

  background-color: ${colors.YELLOW};
`;

const TitleArea = styled.div`
  height: 4rem;
  width: 100%;
  display: flex;
  justify-content: space-evenly;
  align-items: center;
`;

const SmallTitle = styled.div`
  font-family: 'Lilita One', sans-serif;
  font-size: 22px;
`;

const SmallLogo = styled.img`
  width: 3rem;
`;

const Image = styled.img`
  width: 7rem;
`;

const Carousel = styled.div`
  width: 95%;
  height: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
`

const TextRender = styled.div`
  font-family: 'Cabin', sans-serif;
  font-size: 18px;
  margin: .25rem 0;
`

export default ExpandedCardView;
