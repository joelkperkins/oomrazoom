export default {
  WHITE: "#FFFEEE",
//yellow
  YELLOW: "#FFD972",
// orange
  ORANGE: "#F38F6F",
// pink
  PINK: "#A43B74",
// purple
  PURPLE: "#490053",
}