import React, { useState, useEffect, useRef} from "react";
import { useHistory } from "react-router-dom";

// libraries
import styled from 'styled-components';
import { motion } from "framer-motion"
import { shuffle } from "lodash";

// icons, logos, and design
import {MdMail, MdPerson} from 'react-icons/md';
import colors from '../design/colors';
import {AiFillLinkedin, AiOutlineGitlab} from 'react-icons/ai';
import {IoIosSend} from 'react-icons/io'
import {FiAtSign} from 'react-icons/fi'

const ContactView = ({
  className,
  controls,
  primary,
  emailContact,
  setEmailContact,
  email,
  setEmail,
  name,
  setName,
  message,
  setMessage,
  requestResume,
  setRequestResume,
  sendContactRequest
}) => {

  const ROTATION_TIMER = 3100;
  const timerRef = useRef(null);
  const returnRef = useRef(null);
  const history = useHistory();

  const initialColors = [
    { 
      color: colors.ORANGE,
      icon: <AiFillLinkedin size="3rem" color="white" />,
      onClick: () => {
        const win = window.open('https://www.linkedin.com/in/joelkperkins/', '_blank');
        win.focus();
      },
    },
    {
      color: colors.WHITE,
      icon: <AiOutlineGitlab size="3rem" color="black" />,
      onClick: () => {
        const win = window.open('https://github.com/joelkperkins', '_blank');
        win.focus();
      },
    },
    {
      color: colors.PURPLE,
      icon: <Col><Text>Request</Text><br/><Text>Resume</Text></Col>,
      onClick: (x) => setRequestResume(x),
    },
    {
      color: colors.YELLOW,
      icon: <IoIosSend size="3rem" color="black" />,
      onClick: (x) => setEmailContact(x),
    },
  ];

  const spring = {
    type: "spring",
    damping: 20,
    stiffness: 300
  };


  const [currentColors, setCurrentColors] = useState(initialColors);

  useEffect(() => {
    timerRef.current = setTimeout(() => setCurrentColors(shuffle(currentColors)), ROTATION_TIMER);

    return () => {
      clearTimeout(timerRef.current);
    }
  }, [currentColors]);

  if (primary !== 'contact') {
    return (
      <Container 
        id="contact-me"
        animate={controls}
        className={className}
        initial={false}
        whileHover={{ scale: 1.1 }} 
        onClick={() => history.push('/contact')}
      >
        <ButtonContainer>
          <MdMail size="3rem" color={colors.WHITE}/>
          <Title>Contact</Title>
        </ButtonContainer>
      </Container>
    )
  } else {
    return (
      <Container 
        id="contact-me"
        animate={controls}
        className={className}
        initial={false}
      >
        <ButtonContainer onClick={() => returnRef.current ? returnRef.current() : null}>
          <MdMail size="3rem" color={colors.WHITE}/>
          <Title>Contact</Title>
        </ButtonContainer>
        
        <ContactOptionsContainter>
          {!emailContact && !requestResume &&
            <ul style={{
              position: 'relative', 
              padding: 0, 
              display: 'flex', 
              justifyContent: 'center',
              alignItems: 'center',
              width: '100%', 
              flexWrap: 'wrap', 
            }}>
              {currentColors.map(color => (
                <motion.li
                  key={color.color}
                  layoutTransition={spring}
                  onClick={() => {
                    returnRef.current = () => {
                      color.onClick(false)};
                    color.onClick(true)}}
                  style={{ 
                    backgroundColor: color.color,
                    listStyle: 'none',
                    width: `${document.getElementById('root').offsetWidth / 3}px`,
                    height: `${document.getElementById('root').offsetHeight / 5.5}px`,
                    padding: 0,
                    margin: '1rem',
                    borderRadius: '4px',
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center'
                  }}
                >
                  {color.icon}
                </motion.li>
              ))}
            </ul>
          }
          {emailContact && !requestResume &&
            <EmailContainer>
              <Text color={colors.ORANGE}>Send Us an Email!</Text>
              <Row>
                <MdPerson size='2rem' color={colors.WHITE} />
                <Input required={true} placeholder="your name*" value={name} onChange={(e) => setName(e.target.value)} />
              </Row>
              <Row>
                <FiAtSign size='2rem' color={colors.WHITE} />
                <Input required={true} placeholder="your email*" value={email} onChange={(e) => setEmail(e.target.value)} />
              </Row>
              <Row>
                <TextArea required={true} placeholder="Your message here... (*required)" value={message} onChange={(e) => setMessage(e.target.value)} />
              </Row>
              <Row onClick={() => sendContactRequest()}>
                <IoIosSend size='4rem' color={name.length > 0 && email.length > 0 && message.length > 0 ? colors.WHITE : colors.PINK} />
              </Row>
            </EmailContainer>
          }
          {!emailContact && requestResume &&
            <EmailContainer>
              <Row>
                <Text color={colors.ORANGE}>Request Our Resume!</Text>
              </Row>
              <Row>
                <FiAtSign size='2rem' color={colors.WHITE} />
                <Input placeholder="email" value={email} onChange={(e) => setEmail(e.target.value)} />
              </Row>
              <Row>
              </Row>
              <Row onClick={() => sendContactRequest()}>
                <IoIosSend size='4rem' color={email.length > 0 ? colors.WHITE : colors.PINK} />
              </Row>
            </EmailContainer>
          }
        </ContactOptionsContainter>
      </Container>
    )
  }
};

// main button
const Container = styled(motion.div)`
  z-index: 3;
  width: 100%;
  height: 100%;
  
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;

  background: ${colors.PINK};
  border-radius: 4px;
  border: none;
`

const EmailContainer = styled.div`
  height: 100%;
  width: 100%;

  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  align-items: center;
`

const ButtonContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  margin-top: 1.5rem;
`

const Title = styled.div`
  font-family: 'Lilita One', sans-serif;
  font-size: 48px;
  color: ${colors.WHITE};

  &:hover {
    cursor: pointer;
  }
`

const ContactOptionsContainter = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-start;
  align-items: center;
`

const Row = styled.div`
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  width: 15rem;
  height: 4rem;
`

const Col = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 5rem;
`

const Input = styled.input`
  height: 2rem;
  border: none;
  border-radius: 4px;
  font-family: 'Cabin', 'sans-serif';
  font-size: 18px;
`

const TextArea = styled.textarea`
  width: 100%;
  height: ${`${document.getElementById('root').offsetHeight / 15}px`};
  border: none;
  border-radius: 4px;
  margin: 1rem 0;
  text-align: center;
`

const Text = styled.div`
  font-family: 'Lilita One', 'sans-serif';
  font-size: 26px;
  font-style: bold;
  color: ${props => props.color ? props.color : colors.WHITE};
`

export default ContactView;
