import React, { useRef, useEffect } from "react";
import { useHistory } from "react-router-dom";

// libraries
import styled from 'styled-components';
import { motion, useTransform } from "framer-motion";

// logos and icons
import Logo from '../design/primary.png';
import {FaLongArrowAltDown, FaLongArrowAltUp, FaArrowsAltV} from 'react-icons/fa'
import colors from '../design/colors';


const ButtonView = ({y, contactControls, portfolioControls, className, primary}) => {

  const history = useHistory();

  const currY = useRef(null);
  const lastY = useRef(null);
  const dragging = useRef(false);

  const buttonTextColor = useTransform(
    y,
    [-100, 0, 100],
    [colors.PINK, colors.PURPLE, colors.ORANGE]
  )

  useEffect(() => {
    const updateLocation = async () => {
      currY.current = y.get();
      console.log(currY.current);

      if (currY.current > 0 && currY.current - lastY.current > 5 && dragging.current === true) { // down

          lastY.current = currY.current;
          
          if (portfolioControls) {
            portfolioControls.start({
              y: Math.floor(currY.current)
            });
          }
          
          if (contactControls) {
            contactControls.start({
              y: Math.floor(currY.current)
            });
          }

          if (currY.current > 30 && primary !== undefined) {
            lastY.current = null;
            setTimeout(() => history.push('/'), 200);
          } else if (currY.current > 30 && primary === undefined) {
            lastY.current = null;
            setTimeout(() => history.push('/portfolio'), 200);
          }

      } else if (currY.current < 0 && lastY.current - currY.current > 5 && dragging.current === true) { // up

        lastY.current = currY.current;
        
        if (contactControls) {
          contactControls.start({
            y: Math.floor(currY.current)
          });
        }

        if (portfolioControls) {
          portfolioControls.start({
            y: Math.floor(currY.current)
          });
        }

        if (currY.current < -30 && primary !== undefined) {
          lastY.current = null;
          setTimeout(() => history.push('/'), 200);
        } else if (currY.current < -30 && primary === undefined) {
          lastY.current = null;
          setTimeout(() => history.push('/contact'), 200);
        }

      } else if (dragging.current === false) {

        lastY.current = null;

        if (contactControls) {
          contactControls.start({
            y: 0,
            transition: { duration: .1 },
          });
        }

        if (portfolioControls) {
          portfolioControls.start({
            y: 0,
            transition: { duration: .1 },
          });
        }
      }
    }

    const unsubscribeY = y.onChange(updateLocation);

    return () => {
      unsubscribeY()
    }
  
  }, [contactControls, history, portfolioControls, primary, y]);
  
  return (
    <DashButton
      primary={primary}
      className={className}
      buttonTextColor={buttonTextColor.current}
      whileHover={{ scale: 1.1 }} 
      whileTap={{ scale: 0.9 }}
      drag="y"
      dragConstraints={{ top: -5, bottom: 5 }}
      dragElastic={0.45}
      style={{ y, color: buttonTextColor }}
      onDragStart={() => {
        dragging.current = true;
      }}
      onDragEnd={() => {
        dragging.current = false;
      }}
    >
      <ButtonContainer onClick={() => primary === undefined ? null : history.push('/')}>
        {primary === undefined && <FaArrowsAltV size="3rem" color={colors.PURPLE} />}
        {primary === 'contact' && <FaLongArrowAltDown size="3rem" color={colors.PURPLE} />}
        {primary === 'portfolio' && <FaLongArrowAltUp size="3rem" color={colors.PURPLE} />}
        <LogoContainer src={Logo} alt='oomrazoom, small company logo' onMouseDown={(e) => e.preventDefault()} />
      </ButtonContainer>
    </DashButton>
  )
};
// main button
const DashButton = styled(motion.div)`
  z-index: 3;
  width: 85%;
  min-height: 15%;
  max-height: 25%;

  ${props => props.primary === undefined && `margin: ${document.getElementById('root').offsetHeight / 4}px 0;`}
  ${props => props.primary === 'contact' && `margin-bottom: ${document.getElementById('root').offsetHeight / 5.5}px;`}
  ${props => props.primary === 'contact' && 'margin-top: 1rem;'}
  ${props => props.primary === 'portfolio' && `margin-top: ${document.getElementById('root').offsetHeight / 5.5}px;`}
  ${props => props.primary === 'portfolio' && 'margin-bottom: 1rem;'}
  
  display: flex;
  justify-content: flex-end;
  align-items: center;

  background: ${colors.YELLOW};
  border-radius: 4px;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.1);
  border: none;

  font-family: 'IM Fell DW Pica', serif;
  font-size: 48px;
  /* color: ${props => props.buttonTextColor}; */


  &:hover {
    cursor: pointer;
  }
`

const ButtonContainer = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: space-evenly;
  align-items: center;
`

const LogoContainer = styled.img`
  position: relative;
  width: calc(100% - 7rem);
`

export default ButtonView;
