import React, {useState, useEffect} from 'react';
import './App.css';

// animation
import {
  useMotionValue,
  AnimatePresence,
  useAnimation,
} from "framer-motion"

// router
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

// containers
import DefaultContainer from './containers/Default.container';

// views
import ButtonView from './buttons/Button.view';
import ContactView from './contact/Contact.view';
import PortfolioView from './portfolio/Portfolio.view';

// libraries
import emailjs from 'emailjs-com';

const App = () => {

  const buttonY = useMotionValue(0);

  // contact controls
  const contactControls = useAnimation();
  // portfoio controls
  const portfolioControls = useAnimation();

  // contact drawer state
  const [emailContact, setEmailContact] = useState(false);
  const [requestResume, setRequestResume] = useState(false);

  // contact state
  const [email, setEmail] = useState('');
  const [name, setName] = useState('');
  const [message, setMessage] = useState('');

  const sendContactRequest = () => {
    if (emailContact === true) {
      if ((/^[\w-+.]+@([\w-]+\.)+[\w-]{2,6}$/.test(email))) {
        emailjs.send('mailgun', 'template_Zs4bPelN', {
          to_name: 'OOMRAZOOM',
          from_name: name,
          message_html: message,
          reply_to: email
        }, 'user_1rC9HW2iODZ4lQ7zvKWnI');
        setEmail('');
        setName('');
        setMessage('');
        setEmailContact(false);
      }
    } else if (requestResume === true) {
      if ((/^[\w-+.]+@([\w-]+\.)+[\w-]{2,6}$/.test(email))) {
        emailjs.send('mailgun', 'resume', {
          to_name: 'OOMRAZOOM',
        }, 'user_1rC9HW2iODZ4lQ7zvKWnI');
        setEmail('');
        setRequestResume(false);
      }
    }
  }

    // get screen dimensions
    useEffect(() => {
      const getWindowDimensions = () => {
        // const w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        const h = window.innerheight || document.documentElement.clientHeight || document.body.clientHeight;
        // const dimensionType = w > 1024 ? 'big' : 'small';
        document.documentElement.style.setProperty('--vh', `${h * 0.01}px`);
      }
  
      console.log("[BOOT] Setting screen dimensions");
      setTimeout(() => getWindowDimensions(), 500);
  
      let listener = window.addEventListener('resize', () => setTimeout(() => getWindowDimensions(), 500));
  
      return () => {
        window.removeEventListener('resize', listener);
      }
  
    }, []);

  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <DefaultContainer className="App" y={buttonY}>
            <AnimatePresence exitBeforeEnter>
              <PortfolioView className="A" key="A" controls={portfolioControls}/>
              <ButtonView className="B" key="B" y={buttonY} contactControls={contactControls} portfolioControls={portfolioControls} primary={undefined} />
              <ContactView className="C" key="C" controls={contactControls} />
            </AnimatePresence>
          </DefaultContainer>
        </Route>
        <Route path="/contact">
          <DefaultContainer className="App" y={buttonY} primary='contact'>
            <ButtonView className="B" y={buttonY} contactControls={contactControls} primary='contact'/>
            <ContactView
              className="C"
              controls={contactControls}
              primary='contact'
              emailContact={emailContact}
              setEmailContact={(x) => setEmailContact(x)}
              email={email}
              setEmail={setEmail}
              name={name}
              setName={setName}
              message={message}
              setMessage={setMessage}
              requestResume={requestResume}
              setRequestResume={(x) => setRequestResume(x)}
              sendContactRequest={sendContactRequest}
            />
          </DefaultContainer>
        </Route>
        <Route path="/portfolio">
          <DefaultContainer className="App" y={buttonY} primary='portfolio'>
            <PortfolioView
              className="A"
              controls={portfolioControls}
              primary='portfolio'
            />
            <ButtonView className="B" y={buttonY} portfolioControls={portfolioControls} primary='portfolio'/>
          </DefaultContainer>
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
